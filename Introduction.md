#NEURAL NETWORK
A NEURAL NETWORK basically looks for the best line that separates a point(s).

A boundary line = w1x1 + w2x2 + b = Wx + b = 0, 
W = (w1,w2) and x = (x1,x2) 

if we have more data than just two columns we work on n-dimension data where n > 2
Wx + b = w1x1 + w2x2 + ... + wnxn + b = 0
W = (w1,w2,...,wn) and x = (x1,x2,...,xn)

Perceptrons: encoding of our equation into a graph. The Perceptron takes the new points and plots it to check on which side of the line the points lie.

*Perceptrons* can be seen as a combination of nodes. Where the first node calculates a (linear) equation, the second applies step function to the result. See ImagePerceptron.png.

##Perceptrons as Logical Operators

*AND OPERATOR* takes two inputs (true or false). Can only be true if both inputs are true.

*OR OPERATOR* takes two inputs (true or false). Can always be true if any of the inputs is true.

The two ways to go from an AND perceptron to an OR perceptron are: Increase the Weights and Decrease the Magnitude of the Bias.

*NOT OPERATOR* only cares about one input. The operation returns a 0 if the input is 1 and a 1 if it's a 0. The other inputs to the perceptron are ignored.

In order to fix a line so that it can move closer to of further from a point, check the workings of PerceptronTrick.png and PerceptronPseudo.png in WeightsAndBias Folder.

